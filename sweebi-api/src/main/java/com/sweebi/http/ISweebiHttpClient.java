/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 3 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */
package com.sweebi.http;

import com.sweebi.data.ISweebiData;
import com.sweebi.data.ISweebiImage;
import com.sweebi.exceptions.SweebiHttpException;

/**
 * interface for HTTP clients.
 * @author Vincent ROSSIGNOL
 * @since com.idopic.sweebi-api version 0.0.1
 * @version 0.0.1
 */
public interface ISweebiHttpClient 
{

    /**
     * type to be used in requests with JSON content.
     */
    public static final String JSON_CONTENT = "application/json";

    /**
     * type to be used in request with JPEG content.
     */
    public static final String JPEG_CONTENT = "image/jpeg";

    /**
     * header to be used to Idopic's token.
     */
    public static final String IDOPIC_TOKEN = "Idopic-Token";

    /**
     * sends a HTTP GET request to Sweebi's server and returns
     * 	the return result as a {@link ISweebiData}'s instance.
     * @param path request's path.
     * @return a valid {@link ISweebiData}'s instance.
     * @throws SweebiHttpException if a HTTP error occurred.
     */
    ISweebiData get( String path ) throws SweebiHttpException;

    /**
     * sends a HTTP POST request to Sweebi's server and returns the response
     * 	 content as a {@link ISweebiData}'s instance.
     * @param path request's path
     * @param content content to be sent as JSON.
     * @return a valid {@link ISweebiData}'s instance.
     * @throws SweebiHttpException if a HTTP or I/O error occurred.
     */
    ISweebiData post( String path, ISweebiData content ) throws SweebiHttpException;


    /**
     * sends the given image to Sweebi's server and returns the response
     * 	content as a {@link ISweebiImage}'s instance.
     * @param path request's path.
     * @param image image to be sent.
     * @return a valid {@link ISweebiImage}'s instance.
     * @throws SweebiHttpException if a HTTP or I/O error occurred.
     */
    ISweebiData put( String path, ISweebiImage image ) throws SweebiHttpException;

    /**
     * sends a HTTP PUT request (using SSL) to Sweebi's server
     * 	and returns the response content as a {@link ISweebiData}'s
     * 	instance.
     * @param path request's path.
     * @param content content to be sent as JSON. 
     * @return a valid IS
     * @throws SweebiHttpException
     */
    ISweebiData putSSL( String path, ISweebiData content ) throws SweebiHttpException;

    /**
     * sends a HTTP POST request (using SSL) to Sweebi's server
     * 	and returns the response content as a {@link ISweebiData}'s
     * 	instance.
     * @param path request's path
     * @param content content to be sent as JSON.
     * @return a valid {@link ISweebiData}'s instance.
     * @throws SweebiHttpException if a HTTP or I/O error occurred.
     */
    ISweebiData postSSL( String path, ISweebiData content ) throws SweebiHttpException;

    /**
     * sets the Idopic token to be used.
     * @param token the token to be used in request, or <code>null</code>
     * 	if no session exists.
     */
    void setToken( String token );

}
