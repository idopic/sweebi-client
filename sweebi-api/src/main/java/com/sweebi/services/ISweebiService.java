/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */
package com.sweebi.services;

import com.sweebi.data.ISweebiManager;
import com.sweebi.http.ISweebiHttpClient;

/**
 * generic interface for Sweebi's services.
 * @author Vincent ROSSIGNOL
 * @since com.idopic.sweebi-api version 0.0.1
 * @version 0.0.1
 */
public interface ISweebiService 
{	
    /**
     * HTTP client associated with the current service.
     * @return a valid {@link ISweebiHttpClient}'s instance.
     */
    ISweebiHttpClient client();

    /**
     * associated Sweebi manager.
     * @return a valid {@link ISweebiManager}'s instance;
     */
    ISweebiManager manager();

}
