/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */
package com.sweebi.services.impl;

import org.apache.log4j.Logger;

import com.sweebi.data.ISweebiData;
import com.sweebi.data.ISweebiManager;
import com.sweebi.exceptions.SweebiHttpException;
import com.sweebi.http.ISweebiHttpClient;
import com.sweebi.services.AbstractSweebiService;

/**
 * Sweebi's server information.
 * @author Vincent ROSSIGNOL
 * @since com.idopic.sweebi-api version 0.0.1
 * @version 0.0.1
 */
public class SweebiServerService extends AbstractSweebiService 
{

    /**
     * internal logger.
     */
    private static final Logger LOGGER = Logger.getLogger( SweebiServerService.class );

    /**
     * base path on Sweebi server.
     */
    public static final String PATH = "server";
    
    /**
     * constructor.
     * @param client Sweebi's HTTP client.
     */
    public SweebiServerService( ISweebiHttpClient client ) 
    {
        super( client, PATH );
    }

    /**
     * constructor.
     * @param manager Sweebi's manager.
     * @param client Sweebi's HTTP client.
     */
    public SweebiServerService(ISweebiManager manager, ISweebiHttpClient client ) 
    {
        super( manager, client,  PATH);
    }

    /**
     * retrieves server's information from Sweebi.
     * @return a valid {@link ISweebiData}'s instance.
     * @throws SweebiHttpException if a HTTP error occurred.
     */
    public ISweebiData info() throws SweebiHttpException
    {
        return this.client().get( this.path() + "/info" );
    }

    /**
     * tests if Sweebi server can be reached.
     * @return <code>true</code> if server is reachable, <code>false</code>
     * 	otherwise.
     */
    public boolean isAlive()
    {
        ISweebiData pong;
        try 
        {
            pong = this.ping();
        } 
        catch (SweebiHttpException e) 
        {
            LOGGER.debug( "HTTP exception detected while pinging server", e );
            return false;
        }
        return pong != null; 
    }

    /**
     * send a PING to Sweebi server.
     * @return a valid {@link ISweebiData}'s instance.
     * @throws SweebiHttpException
     */
    public ISweebiData ping() throws SweebiHttpException
    {
        return this.client().get( this.path() + "/ping" );
    }
}
