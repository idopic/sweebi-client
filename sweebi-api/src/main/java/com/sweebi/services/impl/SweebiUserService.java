/*
 * SweebiUserService.java
 * 
 * Copyright (c) 2014, Idopic. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package com.sweebi.services.impl;

import java.util.List;

import com.sweebi.data.ISweebiData;
import com.sweebi.data.ISweebiManager;
import com.sweebi.data.impl.SweebiData;
import com.sweebi.exceptions.SweebiHttpException;
import com.sweebi.http.ISweebiHttpClient;
import com.sweebi.services.AbstractSweebiService;

/**
 * "user" service for Sweebi server.
 * @author Vincent ROSSIGNOL
 * @since com.idopic.sweebi-api version 0.0.1
 * @version 0.0.1
 */
public class SweebiUserService extends AbstractSweebiService 
{

    /**
     * base path for "users" service.
     */
    public static final String PATH = "users";
 
    /**
     * constructor.
     * @param manager a valid {@link ISweebiManager}'s instance.
     * @param client a valid {@link ISweebiHttpClient}'s instance.
     */
    public SweebiUserService(ISweebiManager manager, ISweebiHttpClient client) 
    {
        super(manager, client, PATH );
    }

    /**
     * constructor
     * @param client a valid {@link ISweebiHttpClient}'s instance.
     */
    public SweebiUserService(ISweebiHttpClient client) 
    {
        super(client, PATH );
    }

    /**
     * creates a new Sweebi's user.
     * @param mail new user's mail.
     * @param fullname new user's name.
     * @param password new user's password.
     * @return server's response as {@link ISweebiData}'s instance.
     * @throws SweebiHttpException if a HTTP or I/O error occurred.
     */
    public ISweebiData create( String mail, String fullname, String password ) throws SweebiHttpException
    {
        if ( mail == null || mail.isEmpty() )
        {
            throw new IllegalArgumentException( "invalid email: null or empty isn't allowed." );
        }
        
        if ( fullname == null || fullname.isEmpty() )
        {
            throw new IllegalArgumentException( "invalid name: null or empty isn't allowed." );
        }
        
        SweebiData user = new SweebiData();
        user.put( "id",  mail );
        user.put( "display", fullname );
        user.put( "password", SweebiLoginService.hash( password ));
        return this.client().postSSL( this.path() + "/new", user );
    }

    /**
     * requests information about the given user.
     * @param id a valid user' ID.
     * @return a valid {@link ISweebiData}'s instance.
     * @throws SweebiHttpException if a HTTP or a I/O error occurred.
     */
    public ISweebiData userInfo( String id ) throws SweebiHttpException
    {
        this.verify( "uid", id );
        return this.client().get( this.path() + "/" + id + "/info" );
    }
    
    /**
     * requests the private Sweebi list for a given user.
     * @return a valid {@link List} of sweebi's ID.
     * @throws SweebiHttpException if a HTTP or I/O error occurred.
     */
    public List<String> getPrivateSweebis( String uid ) throws SweebiHttpException
    {
        this.verify( "uid", uid );
        ISweebiData data = this.client().get( this.path() + "/" + uid + "/private" );
        return data.getStringList( "owned" );
    }
}
