/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */
package com.sweebi.services.impl;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.sweebi.data.ISweebiData;
import com.sweebi.data.ISweebiManager;
import com.sweebi.data.impl.SweebiData;
import com.sweebi.data.impl.SweebiManager;
import com.sweebi.data.impl.SweebiSession;
import com.sweebi.exceptions.SweebiHttpException;
import com.sweebi.exceptions.SweebiInvalidUserException;
import com.sweebi.http.ISweebiHttpClient;
import com.sweebi.services.AbstractSweebiService;

/**
 * Sweebi "login" service
 * @author Vincent ROSSIGNOL
 * @since com.idopic.sweebi-api version 0.0.1
 * @version 0.0.1
 */
public class SweebiLoginService extends AbstractSweebiService 
{

    /**
     * base path for login service.
     */
    public static final String PATH = "log";    
    
    /**
     * constructor.
     * @param manager Sweebi's manager.
     * @param client HTTP client.
     */
    public SweebiLoginService(ISweebiManager manager, ISweebiHttpClient client) 
    {
        super( manager, client, PATH );  
    }

    /**
     * constructor.
     * @param client HTTP client.
     */
    public SweebiLoginService( ISweebiHttpClient client )
    {
        this( SweebiManager.instance(), client );
    }

    /**
     * send a login request to Sweebi server.
     * @param user a valid Sweebi user ID.
     * @param password user's password.
     * @throws SweebiHttpException if an HTTP or I/O error occurred.
     * @throws SweebiInvalidUserException if server refused user / password.
     */
    public void login( String user, String password ) throws SweebiHttpException, SweebiInvalidUserException
    {
        if ( user == null || user.isEmpty() )
        {
            throw new IllegalArgumentException( "invalid user: null or empty isn't allowed." );
        }
        
        SweebiData login = new SweebiData();
        login.put( "id", user ); 
        login.put( "password", hash( password ));
        ISweebiData response = this.client().postSSL( this.path() + "/in", login );

        if ( response != null && response.getString( "Idopic-Token" ) != null )
        {
            this.manager().session( new SweebiSession( user, response.getString( "Idopic-Token" ) ) );
        }
        else
        {
            throw new SweebiInvalidUserException( "failed loging in user '" + user + "'.");
        }

    }

    /**
     * terminate the current session.
     * @throws SweebiHttpException if an HTTP or I/O error occurred.
     */
    public void logout() throws SweebiHttpException
    {
        this.client().get( this.path() + "/out" );
        this.manager().session( null );
    }
    
    /**
     * encrypts user password.
     * @param string clear password.
     * @return encrypt password as a {@link String}'s instance.
     */
    protected static final String hash( String string )
    {   
        if ( string == null || string.isEmpty() )
        {
            throw new IllegalArgumentException( "invalid string: null or empty isn't allowed." );
        }
        
        MessageDigest digest = null;
        try 
        {
            digest = MessageDigest.getInstance( "SHA-512" );
        } 
        catch (NoSuchAlgorithmException e) 
        {
            throw new IllegalStateException( "algorithm is missing.", e );
        }
        
        try 
        {
            digest.update( string.getBytes( "UTF-8" ) );
        } 
        catch (UnsupportedEncodingException e) 
        {
            throw new IllegalStateException( "UTF-8 isn't supported !!", e );
        }
        
        byte[] value = digest.digest();
        BigInteger number = new BigInteger( value );
        return String.format( "%0" + (value.length << 1) + "X", number );
        
    }
}
