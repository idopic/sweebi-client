/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */
package com.sweebi.services.impl;

import java.util.List;

import com.sweebi.data.ISweebiData;
import com.sweebi.data.ISweebiImage;
import com.sweebi.data.ISweebiManager;
import com.sweebi.data.impl.SweebiData;
import com.sweebi.data.impl.SweebiManager;
import com.sweebi.exceptions.SweebiHttpException;
import com.sweebi.http.ISweebiHttpClient;
import com.sweebi.services.AbstractSweebiService;

/**
 * "canvas" service for Sweebi's server.
 * @author Vincent ROSSIGNOL
 * @since com.idopic.sweebi-api version 0.0.1
 * @version 0.0.1
 */
public class SweebiCanvasService extends AbstractSweebiService 
{

    /**
     * "canvases" service path on Sweebi server.
     */
    public static final String PATH = "canvases";

    /**
     * constructor.
     * @param manager a valid {@link ISweebiManager}'s instance.
     * @param client a valid {@link ISweebiHttpClient}'s instance.
     */
    public SweebiCanvasService(ISweebiManager manager, ISweebiHttpClient client) 
    {
        super(manager, client, PATH);
        
    }

    /**
     * constructor.
     * @param client a valid {@link ISweebiHttpClient}'s instance.
     */
    public SweebiCanvasService(ISweebiHttpClient client) 
    {
        this( SweebiManager.instance(), client );
    }

    /**
     * retrieves and return the public canvas list. 
     * @return a valid List of Sweebi's ID.
     * @throws SweebiHttpException if an HTTP or an I/O error occurred.
     */
    public List<ISweebiData> getPublicCanvases() throws SweebiHttpException
    {
        ISweebiData data = this.client().get( this.path() + "/public/" );
        List<ISweebiData> result = null;
        if ( data != null )
        {
            result = data.getSweebiDataList( "canvases" );
        }
        else
        {
            throw new IllegalStateException( "null Sweebi object has been returned by server." );
        }
        return result;
    }

    /**
     * sends a "create" request for a new Sweebi to server.
     * @param title title for the new Sweebi
     * @param visibility sweebi's visibility (private or public) 
     * @param direction sweebi's direction (stream or chronology)
     * @return server's response as a {@link ISweebiData}'s instance.
     * @throws SweebiHttpException if a HTTP or I/O error occurred.
     */
    public ISweebiData create( String title, String visibility, String direction ) throws SweebiHttpException
    {
      
        
        if ( this.manager().session() == null )
        {
            throw new IllegalStateException( "no Sweebi session exists." );
        }
        
        SweebiData data = new SweebiData();
        data.put( "owner", this.manager().session().user() );
        
        this.verify( "title", title );
        data.put( "title", title );
        
        this.verify( "visibility", visibility );
        data.put( "visibility", visibility );
        
        this.verify( "mode", direction );
        data.put( "mode", direction );

        return this.client().post( this.path() + "/new", data );

    }

    /**
     * requests information about a given Sweebi.
     * @param sid a valid Sweebi's ID.
     * @return a valid {@link ISweebiData}'s instance.
     * @throws SweebiHttpException if an HTTP or I/O error occurred.
     */
    public ISweebiData info( String sid ) throws SweebiHttpException
    {
        this.verify( "sweebi ID", sid );
        return this.client().get( this.path() + "/" + sid + "/info" );
    }
    
    /**
     * adds an image to a given Sweebi.
     * @param image a valid {@link ISweebiImage}'s instance.
     * @param cid a valid Sweebi ID.
     * @throws SweebiHttpException if a HTTP or I/O error occurred.
     */
    public void addImage( ISweebiImage image, String cid ) throws SweebiHttpException
    {
        this.client().put( this.path() + "/" + cid + "/images", image );
    }
}
