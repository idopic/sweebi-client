/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */

package com.sweebi.services;

import com.sweebi.configuration.ISweebiConfiguration;
import com.sweebi.data.ISweebiManager;
import com.sweebi.data.impl.SweebiManager;
import com.sweebi.http.ISweebiHttpClient;

/**
 * a base class for {@link ISweebiService}'s implementations.
 * @author Vincent ROSSIGNOL
 * @since com.idopic.sweebi-api version 0.0.1
 *
 */
public class AbstractSweebiService implements ISweebiService 
{

    /**
     * associated Sweebi configuration.
     */
    private final ISweebiManager manager;

    /**
     * associated Sweebi client.
     */
    private final ISweebiHttpClient client;

    /**
     * base path on Sweebi server.
     */
    private final String path;
    
    /**
     * constructor.
     * @param configuration {@link ISweebiConfiguration}'s instance to be
     * 	associated with the service.
     * @param client {@link ISweebiHttpClient}'s instance to be associated
     * 	with the service.
     * @param service service name (used in URL).
     */
    public AbstractSweebiService( ISweebiManager manager, ISweebiHttpClient client, String service ) 
    {
        super();

        if ( manager == null )
        {
            throw new IllegalArgumentException( "invalid Sweebi manager: null isn't allowed." );
        }
        this.manager = manager;

        if ( client == null )
        {
            throw new IllegalArgumentException( "invalid Sweebi HTTP client: null isn't allowed." );
        }
        this.client = client;
        
        if ( service == null || service.isEmpty() )
        {
            throw new IllegalArgumentException( "invalid service name: null or empty isn't allowed." );
        }
        this.path = "/api-" + manager.configuration().api() + "/" + service;
    }

    /**
     * base path for the current service.
     * @return a valid {@link String} instance.
     */
    protected final String path()
    {
        return this.path;
    }
    
    /**
     * constructor.
     * @param client {@link ISweebiHttpClient}'s instance to be associated
     * 	with the service.
     */
    public AbstractSweebiService( ISweebiHttpClient client, String service )
    {
        this( SweebiManager.instance(), client, service );
    }

    @Override
    public final ISweebiManager manager() 
    {
        return this.manager;
    }

    @Override
    public ISweebiHttpClient client() 
    {
        return this.client;
    }
    
    /**
     * verify if a named value is legal.
     * @param name value's name.
     * @param string value.
     * @throws IllegalArgumentException if the given value isn't valid.
     */
    protected void verify( String name, String string )
    {
        if ( name == null || name.isEmpty() )
        {
            throw new IllegalArgumentException( "invalid name: null or empty isn't allowed." );
        }
        
        if ( string == null )
        {
            throw new IllegalArgumentException( "invalid '" + name + "' value: null isn't allowed." );
        }
        
        if ( string.isEmpty() )
        {
            throw new IllegalArgumentException( "invalid '" + name + "' value: empty string isn't' allowed." );
        }
    }

}
