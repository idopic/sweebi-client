/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */

package com.sweebi.utils;

import com.google.gson.JsonElement;
import com.sweebi.data.ISweebiData;
import com.sweebi.data.impl.SweebiData;

/**
 * useful methods related to {@link ISweebiData}.
 * @author Vincent ROSSIGNOL
 * @since com.idopic.sweebi-api version 0.0.1
 * @version 0.0.1
 */
public class SweebiDataUtils {

	/**
	 * constructor.
	 * @throws UnsupportedOperationException always.
	 */
	protected SweebiDataUtils() throws UnsupportedOperationException
	{
		throw new UnsupportedOperationException( "no SweebiDataUtil instance can be built." );
	}
	
	/**
	 * tests if a member is valid for the given data.
	 * @param data a valid {@link ISweebiData}'s instance.
	 * @param member a valid {@link ISweebiData}'s member name.
	 * @throws IllegalArgumentException if the value corresponding to the
	 * 	member is <code>null</code> or empty.
	 */
	public static final void testValidStringMember( ISweebiData data, String member )
	{
		if ( member == null || member.isEmpty() )
		{
			throw new IllegalArgumentException( "invalid data member name: null or empty isn't allowed." );
		}
		
		if ( data == null )
		{
			throw new IllegalArgumentException( "invalid Sweebi data: null isn't allowed." );
		}
		
		if ( data.getString( member ) == null || data.getString( member ).isEmpty() )
		{
			throw new IllegalArgumentException( "invalid Sweebi object member '" + member + "': null or empty isn't allowed." );
		}
	}
	
	/**
	 * tests if a member is a valid list.
	 * @param data a valid {@link SweebiData}'s instance.
	 * @param member a valid {@link ISweebiData}'s member name.
	 * @throws IllegalArgumentException if no list correspond to the given
	 * 	member.
	 */
	public static final void testValidListMember( SweebiData data, String member )
	{
		testValidMember( data, member );
		
		JsonElement element = data.getJson().get( member );
		
		if ( ! element.isJsonArray() )
		{
			throw new IllegalArgumentException( "member '" + member + "' isn't a list." );
		}
	}
	
	/**
	 * tests if a member exists in the given {@link SweebiData}'s instance.
	 * @param data a valid {@link SweebiData}'s instance.
	 * @param member a valid {@link ISweebiData}'s member name.
	 * @throws IllegalArgumentException if the member doesn't exist.
	 */
	public static final void testValidMember( SweebiData data, String member )
	{
		testValidData( data );
		testValidMemberName( member );
		
		if ( ! data.getJson().has( member ) )
		{
			throw new IllegalArgumentException( "no such member is data: '" + member + "'." );
		}
	}
	
	/**
	 * test if a given member name is valid.
	 * @param name member name to be tested.
	 * @throws IllegalArgumentException if the given name isn't valid.
	 */
	public static final void testValidMemberName( String name )
	{
		if ( name == null )
		{
			throw new IllegalArgumentException( "invalid Sweebi data member name: null isn't allowed." );
		}
		
		if ( name.isEmpty() )
		{
			throw new IllegalArgumentException( "invalid Sweebi data member name: empty isn't allowed." );
		}
	}
	
	/**
	 * tests if the given Sweebi data instance is valid.
	 * @param data {@link ISweebiData}'s instance to be tested.
	 * @throws IllegalArgumentException if the given data are <code>null</code>.
	 */
	public static final void testValidData( ISweebiData data )
	{
		if ( data == null )
		{
			throw new IllegalArgumentException( "invalid Sweebi data: null isn't allowed." );
		}
	}

}
