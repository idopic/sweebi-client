/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */

package com.sweebi.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.CharBuffer;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sweebi.configuration.SweebiConstants;

/**
 * some useful methods releated to GSON objects.
 * @author Vincent ROSSIGNOL
 * @since com.idopic.sweebi-api version 0.0.1
 * @version 0.0.1
 */
public class GsonUtil 
{

    /**
     * constructor.
     * @throws UnsupportedOperationException always.
     */
    protected GsonUtil() throws UnsupportedOperationException 
    {
        throw new UnsupportedOperationException( "no GsonUtil instance can be built." );
    }

    /**
     * builds and returns the {@link JsonObject} corresponding to the given
     * 	JSON string.
     * @param json a valid {@link String}'s instance.
     * @return a valid {@link JsonObject}'s instance.
     */
    public static final JsonObject readJsonString( String json )
    {
        if ( json == null || json.isEmpty() )
        {
            throw new IllegalArgumentException( "invalid JSON: null or empty isn't allowed." );
        }

        JsonParser  parser  = new JsonParser();
        JsonElement element = parser.parse(json);

        if ( ! element.isJsonObject() )
        {
            throw new IllegalArgumentException( "invalid JSON: the string doesn't define a JSON object." );
        }

        return (JsonObject) element;
    }

    /**
     * builds and returns the {@link JsonObject} from the given 
     * 	{@link InputStream}.
     * @param istream a valid {@link InputStream}.
     * @return a valid {@link JsonObject}.
     * @throws IOException if an I/O error occurred.
     */
    public static final JsonObject readObject( InputStream istream ) throws IOException
    {
        if ( istream == null )
        {
            throw new IllegalArgumentException( "invalid input stream: null isn't allowed." );
        }

        InputStreamReader reader = new InputStreamReader( istream );
        String json = null;
        CharBuffer buffer = CharBuffer.allocate( SweebiConstants.MAX_JSON_BUFFER_SIZE );
        try
        {

            while( reader.read( buffer ) != -1 )
            {
                /* no code */
            }
            json = buffer.flip().toString();
        }
        finally
        {
            reader.close();
        }

        return readJsonString(json);
    }

}
