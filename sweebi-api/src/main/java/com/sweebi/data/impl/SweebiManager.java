/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */

package com.sweebi.data.impl;

import java.io.IOException;

import com.sweebi.configuration.ISweebiConfiguration;
import com.sweebi.configuration.impl.SweebiConfiguration;
import com.sweebi.data.ISweebiData;
import com.sweebi.data.ISweebiManager;
import com.sweebi.data.ISweebiSession;
import com.sweebi.http.ISweebiHttpClient;
import com.sweebi.services.impl.SweebiCanvasService;
import com.sweebi.services.impl.SweebiLoginService;
import com.sweebi.services.impl.SweebiServerService;
import com.sweebi.services.impl.SweebiUserService;

/**
 * Sweebi's manager.
 * @author Vincent ROSSIGNOL
 * @since com.idopic.sweebi-api version 0.0.1
 * @version 0.0.1
 */
public class SweebiManager extends SweebiData implements ISweebiManager 
{

    /**
     * default configuration file.
     */
    private static String configuration_file = "/sweebi.json";

    /**
     * default instance.
     */
    private static SweebiManager instance = null;

    /**
     * default instance.
     * @return a valid {@link SweebiManager}'s instance.
     */
    public static SweebiManager instance()
    {
        if ( instance == null )
        {
            instance = new SweebiManager();
        }
        return instance;
    } 

    /**
     * sets the configuration file to be used.
     * <p>
     * this method must be called before any call to {@link SweebiManager#instance()}.
     * </p>
     * @param filename a valid path to configuration file.
     * @throws IllegalStateException if an instance has already been built.
     */
    public static void setConfigurationFile( String filename )
    {
        if ( instance != null )
        {
            throw new IllegalStateException( "an instance has already been built: configuration file must be set before any call to instance() method." );
        }
        configuration_file = filename;
    }

    /**
     * associated Sweebi service.
     */
    private SweebiCanvasService canvases = null;

    /**
     * HTTP client to be associated with services.
     */
    private ISweebiHttpClient client = null;

    /**
     * associated configuration.
     */
    private ISweebiConfiguration configuration = null;

    /**
     * Sweebi login service.
     */
    private SweebiLoginService login = null;


    /**
     * currently selected sweebi.
     */
    private ISweebiData selectedSweebi = null;

    /**
     * Sweebi server service.
     */
    private SweebiServerService server = null;

    /**
     * associated session.
     */
    private ISweebiSession session = null;

    /**
     * associated user service.
     */
    private SweebiUserService users = null;

    /**
     * constructor.
     */
    protected SweebiManager() 
    {
        super();
    }

    @Override
    public SweebiCanvasService canvases() 
    {
        if ( this.canvases == null )
        {
            this.testClient();
            this.canvases = new SweebiCanvasService( this.client );
        }
        return this.canvases;
    }

    @Override
    public ISweebiConfiguration configuration() 
    {
        if ( this.configuration == null )
        {
            try 
            {
                this.configuration = SweebiConfiguration.read( this.getClass().getResourceAsStream( configuration_file ) );
            } 
            catch (IOException e) 
            {
                throw new IllegalStateException( "an I/O error occurred while loading configuration file ('" + configuration_file + "').", e );
            }
        }
        return this.configuration;
    }

    /**
     * associated sweebi client.
     * @return the associated client, or <code>null</code> if no instance
     * 	has been associated yet.
     */
    protected ISweebiHttpClient getClient()
    {
        return this.client;
    }

    @Override
    public ISweebiData getSelectedSweebi() 
    {
        return this.selectedSweebi;
    }

    @Override
    public SweebiLoginService login() 
    {
        if ( this.login == null )
        {
            this.testClient();
            this.login = new SweebiLoginService( this.getClient() );
        }
        return this.login;
    }


    @Override
    public SweebiServerService server() 
    {
        if ( this.server == null )
        {
            this.testClient();
            this.server = new SweebiServerService( this.getClient() );
        }
        return this.server;
    }

    @Override
    public ISweebiSession session() 
    {
        return this.session;
    }

    @Override
    public void session(ISweebiSession session) 
    {
        this.session = session;
        if ( session == null )
        {
            this.client.setToken( null );
        }
        else
        {
            this.client.setToken( session.token() );
        }
    }


    /**
     * set the associated HTTP client.
     * @param client a valid {@link ISweebiHttpClient}'s instance.
     */
    public void setClient( ISweebiHttpClient client )
    {
        this.client = client;
    }

    @Override
    public void setSelectedSweebi(ISweebiData sweebi) 
    {
        this.selectedSweebi = sweebi;
    }

    /**
     * tests if a HTTP client as been associated with the current manager.
     * @throws IllegalStateException if no client is associated.
     */
    private final void testClient()
    {
        if ( this.client == null )
        {
            throw new IllegalStateException( "no ISweebiHttpClient has been associated with the current manager." );
        }
    }

    @Override
    public SweebiUserService users() 
    {
        if ( this.users == null )
        {
            this.testClient();
            this.users = new SweebiUserService( this.getClient() );
        }
        return this.users;
    }


}
