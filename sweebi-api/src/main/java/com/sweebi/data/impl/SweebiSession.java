/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */

package com.sweebi.data.impl;

import com.google.gson.JsonObject;
import com.sweebi.data.ISweebiSession;
import com.sweebi.utils.SweebiDataUtils;

/**
 * @author Vincent ROSSIGNOL
 *
 */
public class SweebiSession extends SweebiData implements ISweebiSession 
{

    /**
     * field name for session's ID.
     */
    public static final String FIELD_SESSION = "session";

    /**
     * field name for user's ID.
     */
    public static final String FIELD_USER = "user";

    /**
     * 
     */
    public SweebiSession( String user, String session ) 
    {
        super();

        if ( user == null || user.isEmpty() )
        {
            throw new IllegalArgumentException( "invalid user ID: null or empty isn't allowed." );
        }
        this.put( "user", user );

        if ( session == null || session.isEmpty() )
        {
            throw new IllegalArgumentException( "invalid session: empty isn't allowed." );
        }
        this.put( "session", session );
    }

    /**
     * @param object
     */
    public SweebiSession(JsonObject object) 
    {
        super(object);
        SweebiDataUtils.testValidStringMember( this, FIELD_USER );
    }

    @Override
    public String user() 
    {
        return this.getString( FIELD_USER );
    }

    @Override
    public String token() 
    {
        return this.getString( FIELD_SESSION );
    }

}
