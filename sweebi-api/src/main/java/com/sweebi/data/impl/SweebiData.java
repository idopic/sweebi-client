/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */
package com.sweebi.data.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.sweebi.data.ISweebiData;
import com.sweebi.utils.GsonUtil;
import com.sweebi.utils.SweebiDataUtils;

/**
 * basic implementation for {@link ISweebiData}.
 * @author Vincent ROSSIGNOL
 * @since com.idopic.sweebi-api version 0.0.1
 * @version 0.0.1
 */
public class SweebiData implements ISweebiData 
{

    /**
     * internal JSON object.
     */
    private final JsonObject gson;

    /**
     * constructor.
     * <p>
     * builds a new empty Sweebi data.
     * </p>
     */
    public SweebiData()
    {
        super();
        this.gson = new JsonObject();
    }

    /**
     * constructor.
     * <p>
     * build a new Sweebi data from the given Gson object.
     * </p>
     * @param object a valid {@link JsonObject}'s instance.
     */
    protected SweebiData( JsonObject object )
    {
        super();
        if ( object == null )
        {
            throw new IllegalArgumentException( "invalid JSON object: null isn't allowed." );
        }
        this.gson = object;
    }

    /**
     * associated Gson object
     * @return a valid {@link JsonObject}'s instance.
     */
    public final JsonObject getJson()
    {
        return this.gson;
    }

    @Override
    public String toJsonString() 
    {
        return this.getJson().toString();
    }

    /**
     * builds a {@link SweebiData}'s instance from the given JSON string.
     * @param json a valid Json {@link String}'s instance.
     * @return a valid {@link SweebiData}'s instance.
     */
    public static SweebiData forJsonString( String json )
    {
        return new SweebiData( GsonUtil.readJsonString(json) );
    }

    /**
     * reads a {@link SweebiData} from a given {@link InputStream}.
     * @param istream a valid {@link InputStream}'s instance.
     * @return a valid {@link SweebiData}'s instance;
     * @throws IOException if an I/O error occurred.
     */
    public static SweebiData read( InputStream istream ) throws IOException
    {
        return new SweebiData( GsonUtil.readObject(istream) );
    }

    @Override
    public String getString( String name )
    {
        String result = null;
        if ( this.getJson().has( name ) && ! this.getJson().get( name ).isJsonNull() )
        {
            result = this.getJson().get( name ).getAsString();
        }
        return result;
    }

    @Override
    public void put(String key, String value) 
    {
        if ( key == null || key.isEmpty() )
        {
            throw new IllegalArgumentException( "invalid name: null or empty isn't allowed." );
        }

        if ( value == null )
        {
            this.getJson().add( key, JsonNull.INSTANCE );
        }
        else
        {
            this.getJson().addProperty( key , value);
        }
    }

    @Override
    public List<String> getStringList(String name) 
    {
        SweebiDataUtils.testValidListMember( this, name );
        List<String> result = new ArrayList<String>();
        JsonArray array = this.getJson().getAsJsonArray( name );
        for ( JsonElement element : array )
        {
            result.add( element.getAsString() );
        }
        return result;
    }

    @Override
    public List<ISweebiData> getSweebiDataList( String name )
    {
        SweebiDataUtils.testValidListMember( this, name );
        List<ISweebiData> result = new ArrayList<ISweebiData>();
        JsonArray array = this.getJson().getAsJsonArray( name );
        for ( JsonElement element : array )
        {
            if ( element.isJsonObject() )
            {
                result.add( new SweebiData( element.getAsJsonObject() ) );
            }
        }
        return result;
    }

}
