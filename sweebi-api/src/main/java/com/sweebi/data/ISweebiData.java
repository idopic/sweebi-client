/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */
package com.sweebi.data;

import java.util.List;

/**
 * general interface for Sweebi's data.
 * @author Vincent ROSSIGNOL
 * @since com.idopic.sweebi-api version 0.0.1
 */
public interface ISweebiData 
{
    /**
     * returns the current instance as a JSON string.
     * @return a valid {@link String}'s instance.
     */
    String toJsonString();

    /**
     * returns a given member as a {@link String}'s instance.
     * @param name the value's name.
     * @return the value as a {@link String}'s instance or <code>null</code>
     * 	if the member cannot be found.
     */
    String getString( String name );

    /**
     * adds a member to the given data instance.
     * @param key the member name.
     * @param value the member value.
     */
    void put( String key, String value );

    /**
     * returns a given member as a {@link List} of {@link String}.
     * @param name a valid member name.
     * @return a valid {@link String}'s list.
     */
    List<String> getStringList( String name );

    /**
     * returns a given member as a {@link List} of {@link ISweebiData}'s 
     * 	instances.
     * @param name a valid member name.
     * @return a valid {@link ISweebiData} list.
     */
    List<ISweebiData> getSweebiDataList( String name );
}
