/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */
package com.sweebi.data;

import com.sweebi.configuration.ISweebiConfiguration;
import com.sweebi.services.impl.SweebiCanvasService;
import com.sweebi.services.impl.SweebiLoginService;
import com.sweebi.services.impl.SweebiServerService;
import com.sweebi.services.impl.SweebiUserService;

/**
 * Sweebi's data.
 * @author Vincent ROSSIGNOL
 * @since com.idopic.sweebi-api version 0.0.1
 * @version 0.0.1
 */
public interface ISweebiManager
{

    /**
     * associated Sweebi configuration.
     * @return a valid {@link ISweebiConfiguration}'s instance.
     */
    ISweebiConfiguration configuration();

    /**
     * current Sweebi session.
     * @return a valid {@link ISweebiSession}'s instance, or <code>null</code>
     * 	if no session has been set yet.
     */
    ISweebiSession session();

    /**
     * sets the current Sweebi session.
     * @param session a {@link ISweebiSession}'s instance or <code>null</code>
     *  if no more session is available.
     */
    void session( ISweebiSession session );

    /**
     * current selected sweebi
     * @return a valid {@link ISweebiData}'s instance which contains 
     * 	information about selected Sweebi, or <code>null</code> if no sweebi
     * 	has been selected yet.
     */
    ISweebiData getSelectedSweebi();

    /**
     * sets the current selected sweebi.
     * @param sweebi a valid {@link ISweebiData}'s instance or 
     * 	<code>null</code> to cancel selection.
     */
    void setSelectedSweebi( ISweebiData sweebi );

    /**
     * Sweebi login server.
     * @return a valid {@link SweebiLoginService}'s instance.
     */
    SweebiLoginService login();

    /**
     * Sweebi server's service.
     * @return a valid {@link SweebiServerService}'s instance.
     */
    SweebiServerService server();

    /**
     * Sweebi's user server.
     * @return a valid {@link SweebiUserService}'s instance.
     */
    SweebiUserService users();

    /**
     * Sweebi service on server.
     * @return a valid {@link SweebiCanvasService}'s instance.
     */
    SweebiCanvasService canvases();

}
