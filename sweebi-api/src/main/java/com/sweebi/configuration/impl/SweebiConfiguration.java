/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */
package com.sweebi.configuration.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.sweebi.configuration.ISweebiConfiguration;
import com.sweebi.data.impl.SweebiData;
import com.sweebi.utils.GsonUtil;

/**
 * Sweebi configuration.
 * @author Vincent ROSSIGNOL
 * @since com.idopic.sweebi-api version 0.0.1
 * @version 0.0.1
 */
public class SweebiConfiguration extends SweebiData implements ISweebiConfiguration 
{

    /**
     * default Sweebi's server name.
     */
    public static final String DEFAULT_SERVER = "www.sweebi.com";

    /**
     * field name used for Sweebi server.
     */
    public static final String FIELD_SERVER = "server";

    /**
     * field name used for Sweebi user.
     */
    public static final String FIELD_USER = "user";

    /**
     * field name used for Sweebi API version.
     */
    private static final String FIELD_API = "api";

    /**
     * reads configuration from the given JSON file.
     * @param file a valid {@link File}'s instance.
     * @return a valid {@link SweebiConfiguration}'s instance.
     * @throws IOException if an I/O error occurred.
     */
    public static SweebiConfiguration read( File file ) throws IOException
    {
        if ( file == null )
        {
            throw new IllegalArgumentException( "invalid file: null isn't allowed." );
        }
        return new SweebiConfiguration( GsonUtil.readObject( new FileInputStream( file ) ) );
    }

    /**
     * reads configuration from the given input stream.
     * @param file a valid {@link InputStream}'s instance.
     * @return a valid {@link SweebiConfiguration}'s instance.
     * @throws IOException if an I/O error occurred.
     */
    public static SweebiConfiguration read( InputStream istream ) throws IOException
    {
        if ( istream == null )
        {
            throw new IllegalArgumentException( "invalid input stream: null isn't allowed." );
        }
        return new SweebiConfiguration( GsonUtil.readObject(istream) );
    }

    /**
     * constructor.
     * <p>
     * builds a new instance with the default values.
     * </p>
     */
    public SweebiConfiguration() 
    {
        this( DEFAULT_SERVER );
    }

    /**
     * constructor.
     * <p>
     * build a new {@link SweebiConfiguration}'s instance with the given 
     *  server.
     * </p>
     * @param server a valid server name or ip with an optional port (e.g.
     *  sweebi:80 or 127.0.0.1 or 192.168.1.1:90)
     */
    public SweebiConfiguration( String server )
    {
        super();
        if ( server == null || server.isEmpty() )
        {
            throw new IllegalArgumentException( "invalid server: null or empty isn't allowed." );
        }
        this.getJson().addProperty( FIELD_SERVER, server );
        this.getJson().add( FIELD_USER, JsonNull.INSTANCE );
    }
    
    /**
     * @param object
     */
    protected SweebiConfiguration(JsonObject object) 
    {
        super(object);

        if ( this.getString( FIELD_SERVER ) == null || this.getString( FIELD_SERVER ).isEmpty() )
        {
            throw new IllegalArgumentException( "invalid Json object: no member " + FIELD_SERVER + " can be detected." );
        }
    }

    @Override
    public String server() 
    {
        return this.getString( FIELD_SERVER );
    }

    @Override
    public String user() 
    {
        return this.getString( FIELD_USER );
    }

    @Override
    public String api() 
    {
        return this.getString( FIELD_API );
    }
}
