/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */
package com.sweebi.configuration;

/**
 * defines some Sweebi's constants.
 * @author Vincent ROSSIGNOL
 * @since com.idopic.sweebi-api version 0.0.1
 * @version 0.0.1
 */
public class SweebiConstants 
{

    /**
     * constructor.
     * @throws UnsupportedOperationException always.
     */
    protected SweebiConstants() 
    {
        throw new UnsupportedOperationException( "no SweebiConstants' instance can be built." );
    }

    /**
     * maximum buffer size for JSON strings.
     */
    public static final int MAX_JSON_BUFFER_SIZE = 10240;

    /**
     * base size for images.
     */
    public static final int IMAGE_BASE_SIZE = 1344;

}
