/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */
package com.sweebi.exceptions;

/**
 * base exception in Sweebi's API
 * @author Vincent ROSSIGNOL
 * @since com.idopic.sweebi-api version 0.0.1
 * @version 0.0.1
 */
public class SweebiException extends Exception 
{

    /**
     * version UID for serialization.
     */
    private static final long serialVersionUID = 6558709595064965169L;

    /**
     * constructor.
     * @param message exception's message.
     */
    public SweebiException(String message) 
    {
        super(message);
    }

    /**
     * constructor.
     * @param message exception's message
     * @param cause exception's cause.
     */
    public SweebiException(String message, Throwable cause) 
    {
        super(message, cause);
    }

}
