/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */
package com.sweebi.exceptions;

import com.sweebi.data.ISweebiData;

/**
 * Sweebi exception class for HTTP errors.
 * @author Vincent ROSSIGNOL
 * @since com.idopic.sweebi-api version 0.0.1
 * @version 0.0.1
 */
public class SweebiHttpException extends SweebiException 
{

    /**
     * version UID for serialization.
     */
    private static final long serialVersionUID = 2497353524300443181L;

    /**
     * associated sweebi data.
     */
    private final ISweebiData data;

    /**
     * constructor.
     * @param message exception's message.
     */
    public SweebiHttpException( String message ) 
    {
        this(message, (ISweebiData) null );
    }

    /**
     * constructor.
     * @param message exception's message.
     * @param cause exception's cause.
     */
    public SweebiHttpException(String message, Throwable cause) 
    {
        this(message, null, cause);
    }

    public SweebiHttpException( String message, ISweebiData data )
    {
        super( message );
        this.data = data;
    }

    public SweebiHttpException( String message, ISweebiData data, Throwable cause )
    {
        super( message, cause );
        this.data = data;
    }

    /**
     * data associated with the HTTP error.
     * <p>
     * this field mainly contains error messages from server.
     * </p>
     * @return associated {@link ISweebiData}'s instance, or <code>null</code>
     * 	if no data are associated with the current exception.
     */
    public final ISweebiData getSweebiData()
    {
        return this.data;
    }
}
