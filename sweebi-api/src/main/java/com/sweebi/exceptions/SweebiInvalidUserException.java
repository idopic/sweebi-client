/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */
package com.sweebi.exceptions;

/**
 * Sweebi exception user for invalid user / password.
 * @author Vincent ROSSIGNOL
 * @since com.idopic.sweebi-api version 0.0.1
 * @version 0.0.1
 */
public class SweebiInvalidUserException extends SweebiException 
{

    /**
     * version UID for serialization.
     */
    private static final long serialVersionUID = 6987408114136021851L;

    /**
     * constructor.
     * @param message exception message
     */
    public SweebiInvalidUserException(String message) 
    {
        super(message);
    }

    /**
     * constructor.
     * @param message exception message.
     * @param cause exception cause.
     */
    public SweebiInvalidUserException(String message, Throwable cause) 
    {
        super(message, cause);
    }

}
