/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 3 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */
package com.sweebi.http;

import com.sweebi.data.ISweebiData;
import com.sweebi.data.ISweebiImage;
import com.sweebi.exceptions.SweebiHttpException;

/**
 * a "stub" implementation for {@link ISweebiHttpClient} which can be used in
 *  unit tests.
 * @author Vincent ROSSIGNOL
 * @since com.sweebi.sweebi-api version 0.0.1
 * @version 0.0.1
 */
public class SweebiHttpStub implements ISweebiHttpClient 
{

    /**
     * constructor.
     */
    public SweebiHttpStub() 
    {
        /* no code */
    }
   
    @Override
    public ISweebiData get(String path) throws SweebiHttpException 
    {
        throw new UnsupportedOperationException( "get isn't implemented." );
    }
  
    @Override
    public ISweebiData post(String path, ISweebiData content) throws SweebiHttpException 
    {
        throw new UnsupportedOperationException( "post isn't implemented." );
    }

    @Override
    public ISweebiData put(String path, ISweebiImage image) throws SweebiHttpException 
    {
        throw new UnsupportedOperationException( "put isn't implemented." );
    }

    @Override
    public ISweebiData putSSL(String path, ISweebiData content) throws SweebiHttpException 
    {
        throw new UnsupportedOperationException( "putSSL isn't implemented." );
    }

    @Override
    public ISweebiData postSSL(String path, ISweebiData content) throws SweebiHttpException 
    {
        throw new UnsupportedOperationException( "postSSL isn't implemented." );
    }

    @Override
    public void setToken(String token) 
    {
        /* no code */
    }

}
