/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */
package com.sweebi.exceptions;

import static org.junit.Assert.*;

import org.junit.Test;

import com.sweebi.exceptions.SweebiException;

public class SweebiExceptionTest {

	@Test
	public void testSweebiExceptionString() 
	{
		String message = "my message";
		SweebiException exception = new SweebiException(message);
		assertEquals( message, exception.getMessage() );
		
		SweebiException notSoGood = new SweebiException( null );
		assertNull( notSoGood.getMessage() );
	}

	@Test
	public void testSweebiExceptionStringThrowable() 
	{
		String message = "my message";
		Exception cause = new Exception();
		
		SweebiException exception = new SweebiException(message, cause);
		assertEquals( message, exception.getMessage() );
		assertSame( cause, exception.getCause() );
		
		SweebiException notSoGood = new SweebiException( null, cause );
		assertNull( notSoGood.getMessage() );
		assertSame( cause, notSoGood.getCause() );
		
		SweebiException useless = new SweebiException( message, null );
		assertEquals( message, useless.getMessage() );
		assertNull( useless.getCause() );
		
		SweebiException realBad = new SweebiException( null, null );
		assertNull( realBad.getMessage() );
		assertNull( realBad.getCause() );
	}

}
