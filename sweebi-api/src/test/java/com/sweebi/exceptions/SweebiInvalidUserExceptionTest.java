package com.sweebi.exceptions;

import static org.junit.Assert.*;

import org.junit.Test;

import com.sweebi.exceptions.SweebiInvalidUserException;

public class SweebiInvalidUserExceptionTest 
{

	@Test
	public void testSweebiInvalidUserExceptionString() 
	{
		String message = "my message";
		
		SweebiInvalidUserException exception = new SweebiInvalidUserException( message );
		assertEquals( message, exception.getMessage() );
	}

	@Test
	public void testSweebiInvalidUserExceptionStringThrowable() 
	{
		String message = "my message";
		Exception cause = new Exception();
		
		SweebiInvalidUserException exception = new SweebiInvalidUserException( message, cause );
		assertEquals( message, exception.getMessage() );	
		assertSame( cause, exception.getCause() );
	}

}
