/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */

package com.sweebi.exceptions;

import static org.junit.Assert.*;

import org.junit.Test;

import com.sweebi.data.impl.SweebiData;
import com.sweebi.exceptions.SweebiHttpException;

public class SweebiHttpExceptionTest 
{

	@Test
	public void testSweebiHttpExceptionString() 
	{
		String message = "my message";
		
		SweebiHttpException exception = new SweebiHttpException( message );
		assertEquals( message, exception.getMessage() );
		
		assertNull( exception.getSweebiData() );
	}

	@Test
	public void testSweebiHttpExceptionStringThrowable() 
	{
		String message = "my message";
		Exception cause = new Exception();
		
		SweebiHttpException exception = new SweebiHttpException( message, cause );
		assertEquals( message, exception.getMessage() );	
		assertSame( cause, exception.getCause() );
		
		assertNull( exception.getSweebiData() );
	}
	
	@Test
	public void testSweebiHttpExceptionStringSweebiData() 
	{
		String message = "my message";
		SweebiData data = new SweebiData();
		
		SweebiHttpException exception = new SweebiHttpException( message, data );
		assertEquals( message, exception.getMessage()  );
		
		assertSame( data, exception.getSweebiData() );
	}
	
	@Test
	public void testSweebiHttpExceptionStringSweebiDataThrowable() 
	{
		String message = "my message";
		Exception cause = new Exception();
		SweebiData data = new SweebiData();
		
		SweebiHttpException exception = new SweebiHttpException( message, data, cause );
		assertEquals( message, exception.getMessage() );	
		assertSame( cause, exception.getCause() );
		
		assertSame( data, exception.getSweebiData() );
	}

}
