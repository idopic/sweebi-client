/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */
package com.sweebi.utils;

import static org.junit.Assert.fail;

import org.junit.Test;

import com.sweebi.data.impl.SweebiData;
import com.sweebi.utils.SweebiDataUtils;

public class SweebiDataUtilsTest 
{


	@Test
	public void testSweebiDataUtils() 
	{
		try
		{
			new SweebiDataUtils();
			fail( "an exception should have been raised." );
		}
		catch ( UnsupportedOperationException e )
		{
			/* success */
		}
	}

	@Test
	public void testTestValidStringMember() 
	{
		SweebiData data = new SweebiData();
		data.put( "validstring",  "a string" );
		data.put( "emptystring", "" );
		data.put( "nullstring", null );
		
		try
		{
			SweebiDataUtils.testValidStringMember( data,  null );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
		
		try
		{
			SweebiDataUtils.testValidStringMember( data,  "" );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
		
		try
		{
			SweebiDataUtils.testValidStringMember( null,  "a member" );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
		
		try
		{
			SweebiDataUtils.testValidStringMember( data,  "nullstring" );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
		
		try
		{
			SweebiDataUtils.testValidStringMember( data,  "emptystring" );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
		
		SweebiDataUtils.testValidStringMember( data, "validstring" );
	}
	
	@Test
	public void testTestValidData()
	{
		try
		{
			SweebiDataUtils.testValidData( null );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
		
		SweebiDataUtils.testValidData( new SweebiData() );
		
	}
	
	@Test
	public void testTestValidMemberName()
	{
		try
		{
			SweebiDataUtils.testValidMemberName( null );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
		
		
		try
		{
			SweebiDataUtils.testValidMemberName( "" );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
		
		SweebiDataUtils.testValidMemberName( "toto" );
		
	}

	@Test
	public void testTestValidMember()
	{
		try
		{
			SweebiDataUtils.testValidMember( new SweebiData(), "member" );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
		
		try
		{
			SweebiDataUtils.testValidMember( new SweebiData(), "" );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
		
		try
		{
			SweebiDataUtils.testValidMember( null, "member" );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
		
		SweebiData data = SweebiData.forJsonString( "{ \"foo\": 2 }" );
		SweebiDataUtils.testValidMember( data, "foo" );
	}
	
	@Test
	public void testTestValidListMember()
	{
		try
		{
			SweebiDataUtils.testValidListMember( new SweebiData(), "list" );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
		
		try
		{
			SweebiDataUtils.testValidListMember( new SweebiData(), "" );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
		
		try
		{
			SweebiDataUtils.testValidListMember( null, "member" );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
		
		SweebiData data = SweebiData.forJsonString( "{ \"foo\": [ 1, 2 ] }" );
		SweebiDataUtils.testValidListMember( data, "foo" );
		
		data = SweebiData.forJsonString( "{ \"foo\": 2 }" );
		try
		{
			SweebiDataUtils.testValidListMember( data, "foo" );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
	}
	
	
	
}
