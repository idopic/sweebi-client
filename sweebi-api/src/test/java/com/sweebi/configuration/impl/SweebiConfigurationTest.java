/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */

package com.sweebi.configuration.impl;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.gson.JsonObject;
import com.sweebi.configuration.impl.SweebiConfiguration;
import com.sweebi.utils.GsonUtil;

public class SweebiConfigurationTest 
{

	/**
	 * instance under test.
	 */
	private SweebiConfiguration configuration = null;
	
	@Before
	public void setUp() throws Exception 
	{
		String path = SweebiConfigurationTest.class.getResource( "/configuration.json" ).getPath();
		this.configuration = SweebiConfiguration.read( new File( path ) ); 
	}

	@After
	public void tearDown() throws Exception 
	{
		this.configuration = null;
	}

	@Test
	public void testSweebiConfiguration() 
	{
		SweebiConfiguration defaultConf = new SweebiConfiguration();
		assertNotNull( defaultConf.server() );
		assertNull( defaultConf.user() );
	}

	@Test
	public void testSweebiConfigurationString()
	{
	    try
	    {
	        new SweebiConfiguration( (String) null );
	        fail( "an exception should have been raised." );
	    }
	    catch ( IllegalArgumentException e )
	    {
	        /* success */
	    }
	    
	    try
        {
            new SweebiConfiguration( "" );
            fail( "an exception should have been raised." );
        }
        catch ( IllegalArgumentException e )
        {
            /* success */
        }
	    
	    SweebiConfiguration conf2 = new SweebiConfiguration( "myserver" );
	    assertEquals( "myserver", conf2.server() );
	    assertNull( conf2.user() );
	}
	
	@Test
	public void testSweebiConfigurationJsonObject() 
	{
		try
		{
			new SweebiConfiguration( (JsonObject) null );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
		
		try
		{
			new SweebiConfiguration( GsonUtil.readJsonString( "{ \"foo\": 123 }" ) );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
 	 }

	@Test
	public void testServer() 
	{
		assertEquals( "docker03.home.rh", configuration.server() );
	}

	@Test
	public void testUser() 
	{
		assertEquals( "vincent@sweebi.com", configuration.user() );
	}

	@Test
	public void testReadFile() throws IOException 
	{
		try
		{
			SweebiConfiguration.read( (File) null );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
		
		try
		{
			SweebiConfiguration.read( new File( "/foo/bar" ) );
			fail( "an exception should have been raised." );
		}
		catch ( IOException e )
		{
			/* success */
		}
	}

	@Test
	public void testReadInputStream() throws IOException
	{
		try
		{
			SweebiConfiguration.read( (InputStream) null );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
	}
}
