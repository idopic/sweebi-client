/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */

package com.sweebi.configuration;

import static org.junit.Assert.*;

import org.junit.Test;

import com.sweebi.configuration.SweebiConstants;

public class SweebiConstantsTest {

	@Test
	public void testSweebiConstants() 
	{
		try
		{
			new SweebiConstants();
			fail( "an exception should have been raised." );
		}
		catch ( UnsupportedOperationException e )
		{
			/* success */
		}
	}

}
