/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */

package com.sweebi.data.impl;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.sweebi.data.impl.SweebiSession;

public class SweebiSessionTest 
{

	private SweebiSession session = null;
	
	@Before
	public void setUp() throws Exception 
	{
		this.session = new SweebiSession( "me@nowhere.net", "12345" );
	}

	@After
	public void tearDown() throws Exception 
	{
		this.session = null;
	}

	@Test
	public void testSweebiSessionStringString() 
	{
		try
		{
			new SweebiSession( null, "12345" );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
		
		try
		{
			new SweebiSession( "", "12345" );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
		
		try
		{
			new SweebiSession( "me@nowhere.net", null );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
		
		try
		{
			new SweebiSession( "me@nowhere.net", "" );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
	}

	@Test
	public void testSweebiSessionJsonObject() 
	{
		try
		{
			new SweebiSession( null );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
	}

	@Test
	public void testUser() 
	{
		assertEquals( "me@nowhere.net", session.user() );
	}

	@Test
	public void testToken() 
	{
		assertEquals( "12345", session.token() );
	}

}
