/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */
package com.sweebi.data.impl;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.gson.JsonSyntaxException;
import com.sweebi.data.impl.SweebiData;

public class SweebiDataTest 
{
	/**
	 * a valid JSON string.
	 */
	public static final String JSON_STRING = "{ \"number\": 1973, \"string\":\"a string\", \"list\": [ \"one\", 2 ] }";
	
	/**
	 * instance to be tested.
	 */
	private SweebiData data = null;
	
	@Before
	public void setUp() throws Exception 
	{
		this.data = SweebiData.forJsonString( JSON_STRING );
	}

	@After
	public void tearDown() throws Exception 
	{
		this.data = null;
	}

	@Test
	public void testSweebiData()
	{
		SweebiData data = new SweebiData();
		assertNotNull( data );
	}

	@Test
	public void testSweebiDataJsonObject() 
	{
		try
		{
			new SweebiData( null );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
	}

	@Test
	public void testGetJson() 
	{
		assertNotNull( data.getJson() );
		assertEquals( 1973, data.getJson().get( "number").getAsInt() );
		assertEquals( "a string", data.getJson().get( "string" ).getAsString() );
	}

	@Test
	public void testToJsonString() 
	{
		assertNotNull( data.toJsonString() );
		assertFalse( data.toJsonString().isEmpty() );

		String json = data.toJsonString();
		SweebiData reread = SweebiData.forJsonString( json );
		assertNotNull( reread );
		assertEquals( "a string", reread.getString( "string" ) );
	}

	@Test
	public void testForJsonString() 
	{
		try
		{
			SweebiData.forJsonString( null );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
		
		try
		{
			SweebiData.forJsonString( "" );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
		
		try
		{
			SweebiData.forJsonString( "invalid json string" );
			fail( "an exception should have been raised." );
		}
		catch ( JsonSyntaxException e )
		{
			/* success */
		}
	}

	@Test
	public void testRead() throws IOException
	{
		try
		{
			SweebiData.read( null );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
		
		SweebiData read = SweebiData.read( SweebiDataTest.class.getResourceAsStream( "/valid.json" ) );
		assertEquals( "file", read.getString( "type" ) );
		assertEquals( "2014", read.getString( "year" ) );
	}
	
	@Test
	public void testGetString() 
	{
		assertEquals( "a string", data.getString( "string" ) );
		assertNull( data.getString( "foo" ) );
		assertEquals( "1973", data.getString( "number" ) );
	}

	@Test
	public void testPutString()
	{
		assertNull( data.getString( "newmember" ) );
		
		try
		{
			data.put( null , "new value" );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
		
		try
		{
			data.put( "" , "new value" );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
		
		data.put( "newmember", "new value" );
		assertEquals( "new value", data.getString( "newmember" ) );
		
		data.put( "null", null );
		assertNull( data.getString( "null" ) );
	}
	
	@Test
	public void testGetStringList()
	{
		try
		{
			data.getStringList( null );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
		
		try
		{
			data.getStringList( "nosuchlist" );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
		
		List<String> result = data.getStringList( "list" );
		assertNotNull( result );
		assertEquals( "one", result.get( 0 ) );
		assertEquals( "2", result.get( 1 ) );
	}
}
