/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 3 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */

package com.sweebi.services.impl;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.sweebi.data.impl.SweebiManager;
import com.sweebi.exceptions.SweebiHttpException;
import com.sweebi.exceptions.SweebiInvalidUserException;
import com.sweebi.http.SweebiHttpStub;

public class SweebiLoginServiceTest 
{

    private final SweebiHttpStub client = new SweebiHttpStub();
    
    private SweebiLoginService service = null;
    
    @BeforeClass
    public static void before()
    {
        try
        {
            SweebiManager.setConfigurationFile( "/configuration.json" );
        }
        catch ( IllegalStateException e )
        {
            /* not an error */
        }
    }
    
    @Before
    public void setUp() throws Exception 
    {
        this.service = new SweebiLoginService( client );
    }

    @After
    public void tearDown() throws Exception 
    {
        this.service = null;
    }

    @Test
    public void testSweebiLoginServiceISweebiManagerISweebiHttpClient() 
    {
        try
        {
            new SweebiLoginService( SweebiManager.instance(), null );
            fail( "an exception should have been raised." );
        }
        catch ( IllegalArgumentException e )
        {
            /* success */
        }
        
        try
        {
            new SweebiLoginService( null, client );
            fail( "an exception should have been raised." );
        }
        catch ( IllegalArgumentException e )
        {
            /* success */
        }
        
        assertNotNull( new SweebiCanvasService(SweebiManager.instance(), client) );
    }

    @Test
    public void testSweebiLoginServiceISweebiHttpClient() 
    {
        assertNotNull( service );
        
        try
        {
            new SweebiLoginService( null );
            fail( "an exception should have been raised." );
        }
        catch ( IllegalArgumentException e )
        {
            /* success */
        }
    }

    @Test
    public void testLogin() throws SweebiHttpException, SweebiInvalidUserException 
    {
        try 
        {
            service.login( null, "password" );
            fail( "an exception should have been raised." );
        } 
        catch ( IllegalArgumentException e) 
        {
            /* success */
        }
        
        try 
        {
            service.login( "", "password" );
            fail( "an exception should have been raised." );
        } 
        catch ( IllegalArgumentException e) 
        {
            /* success */
        }
        
        try 
        {
            service.login( "user", null );
            fail( "an exception should have been raised." );
        } 
        catch ( IllegalArgumentException e) 
        {
            /* success */
        }
        
        try 
        {
            service.login( "user", "" );
            fail( "an exception should have been raised." );
        } 
        catch ( IllegalArgumentException e) 
        {
            /* success */
        }
        
        try
        {
            service.login( "me@nowhere.com", "aPaSsWoRd" );
            fail( "an exception should have been raised." );
        }
        catch ( UnsupportedOperationException e )
        {
            /* success */
        }
    }

    @Test
    public void testLogout() throws SweebiHttpException 
    {
        try
        {
            service.logout();
            fail( "an exception should have been raised." );
        }
        catch ( UnsupportedOperationException e )
        {
            /* success */
        }
    }

    @Test
    public void testHash() 
    {
        try
        {
            SweebiLoginService.hash( null );
            fail( "an exception should have been raised." );
        }
        catch ( IllegalArgumentException e )
        {
            /* success */
        }
        
        try
        {
            SweebiLoginService.hash( "" );
            fail( "an exception should have been raised." );
        }
        catch ( IllegalArgumentException e )
        {
            /* success */
        }
        
        assertEquals( SweebiLoginService.hash( "passowed" ), SweebiLoginService.hash( "passowed" ) );
    }
    

}
