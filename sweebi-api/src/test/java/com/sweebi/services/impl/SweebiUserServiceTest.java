/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 3 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */

package com.sweebi.services.impl;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.sweebi.data.impl.SweebiManager;
import com.sweebi.exceptions.SweebiHttpException;
import com.sweebi.http.SweebiHttpStub;

/**
 * @author Vincent
 *
 */
public class SweebiUserServiceTest {

    private final SweebiHttpStub client = new SweebiHttpStub();
    
    private SweebiUserService service = null;
    
    @BeforeClass
    public static void before()
    {
        try
        {
            SweebiManager.setConfigurationFile( "/configuration.json" );
        }
        catch ( IllegalStateException e )
        {
            /* not an error */
        }
    }
    
    @Before
    public void setUp() throws Exception 
    {
        this.service = new SweebiUserService( client );
    }

    @After
    public void tearDown() throws Exception 
    {
        this.service = null;
    }

    /**
     * Test method for {@link com.sweebi.services.impl.SweebiUserService#SweebiUserService(com.sweebi.data.ISweebiManager, com.sweebi.http.ISweebiHttpClient)}.
     */
    @Test
    public void testSweebiUserServiceISweebiManagerISweebiHttpClient() 
    {
        try
        {
            new SweebiUserService( SweebiManager.instance(), null );
            fail( "an exception should have been raised." );
        }
        catch ( IllegalArgumentException e )
        {
            /* success */
        }
        
        try
        {
            new SweebiUserService( null, client );
            fail( "an exception should have been raised." );
        }
        catch ( IllegalArgumentException e )
        {
            /* success */
        }
        
        assertNotNull( new SweebiUserService(SweebiManager.instance(), client) );
    }

    /**
     * Test method for {@link com.sweebi.services.impl.SweebiUserService#SweebiUserService(com.sweebi.http.ISweebiHttpClient)}.
     */
    @Test
    public void testSweebiUserServiceISweebiHttpClient() 
    {
        assertNotNull( service );
        
        try
        {
            new SweebiUserService( null );
            fail( "an exception should have been raised." );
        }
        catch ( IllegalArgumentException e )
        {
            /* success */
        }
    }

    /**
     * Test method for {@link com.sweebi.services.impl.SweebiUserService#create(java.lang.String, java.lang.String, java.lang.String)}.
     * @throws SweebiHttpException 
     */
    @Test
    public void testCreate() throws SweebiHttpException 
    {
        try
        {
            service.create( null, "Me YOU", "password" );
            fail( "an exception should have been raised." );
        }
        catch ( IllegalArgumentException e )
        {
            /* success */
        }
        
        try
        {
            service.create( "", "Me YOU", "password" );
            fail( "an exception should have been raised." );
        }
        catch ( IllegalArgumentException e )
        {
            /* success */
        }
        
        try
        {
            service.create( "me@nowhere.net", null, "password" );
            fail( "an exception should have been raised." );
        }
        catch ( IllegalArgumentException e )
        {
            /* success */
        }
        
        try
        {
            service.create( "me@nowhere.net", "", "password" );
            fail( "an exception should have been raised." );
        }
        catch ( IllegalArgumentException e )
        {
            /* success */
        }
        
        try
        {
            service.create( "me@nowhere.net", "Me YOU", null );
            fail( "an exception should have been raised." );
        }
        catch ( IllegalArgumentException e )
        {
            /* success */
        }
        
        try
        {
            service.create( "me@nowhere.net", "Me YOU", "" );
            fail( "an exception should have been raised." );
        }
        catch ( IllegalArgumentException e )
        {
            /* success */
        }
        
        try
        {
            service.create( "me@nowhere.net", "Me You", "password" );
            fail( "an exception should have been raised." );
        }
        catch ( UnsupportedOperationException e )
        {
            /* success */
        }
    }

    /**
     * Test method for {@link com.sweebi.services.impl.SweebiUserService#userInfo(java.lang.String)}.
     * @throws SweebiHttpException 
     */
    @Test
    public void testUserInfo() throws SweebiHttpException 
    {
        try
        {
            service.userInfo( null );
            fail( "an exception should have been raised." );
        }
        catch ( IllegalArgumentException e )
        {
            /* success */
        }
        
        try
        {
            service.userInfo( "" );
            fail( "an exception should have been raised." );
        }
        catch ( IllegalArgumentException e )
        {
            /* success */
        }
        
        try
        {
            service.userInfo( "me@nowhere.net" );
            fail( "an exception should have been raised." );
        }
        catch ( UnsupportedOperationException e )
        {
            /* success */
        }
    }
    
    @Test
    public void testGetPrivateSweebis() throws SweebiHttpException
    {
        try
        {
            service.getPrivateSweebis( null );
            fail( "an exception should have been raised." );
        }
        catch ( IllegalArgumentException e )
        {
            /* success */
        }
        
        try
        {
            service.getPrivateSweebis( "me@nowhere.com" );
            fail( "an exception should have been raised." );
        }
        catch ( UnsupportedOperationException e )
        {
            /* success */
        }
    }

}
