# Sweebi client #

this project proposes a java API for Sweebi's servers.

please notice that for the moment, no Sweebi server has been publish on Internet, but a beta version will soon be release.

for bugs or enhancement, please go to the issue section.

## license ##

![GLGPL logo](http://www.gnu.org/graphics/lgplv3-88x31.png)


* [Lesser General Public License v3](http://www.gnu.org/licenses/lgpl.html)

## information ##


* CI located at https://drone.io/bitbucket.org/idopic/sweebi-client


### maven distibution ###

coming soon.