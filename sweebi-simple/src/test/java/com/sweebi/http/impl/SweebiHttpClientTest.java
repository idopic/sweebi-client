/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */
package com.sweebi.http.impl;

import static org.junit.Assert.*;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.sweebi.configuration.ISweebiConfiguration;
import com.sweebi.configuration.impl.SweebiConfiguration;
import com.sweebi.data.ISweebiData;
import com.sweebi.exceptions.SweebiHttpException;
import com.sweebi.http.impl.SweebiHttpClient;

public class SweebiHttpClientTest 
{

	private static ISweebiConfiguration configuration = null;
	
	@BeforeClass
	public static void before() throws IOException
	{
		configuration = SweebiConfiguration.read( SweebiHttpClientTest.class.getResourceAsStream( "/home.json" ) );
	}
	
	/**
	 * instance under test.
	 */
	private SweebiHttpClient client = null;
	
	@Before
	public void setUp() throws Exception 
	{
		client = new SweebiHttpClient( "http://" + configuration.server() + "/api-" + configuration.api() + "/" );
	}

	@After
	public void tearDown() throws Exception 
	{
		client = null;
	}

	@Test
	public void testSweebiHttpClient() 
	{
		assertNotNull( client );
		
		try
		{
			new SweebiHttpClient( null );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
		
		try
		{
			new SweebiHttpClient( "" );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
	}

	@Test
	public void testGetBaseUrl() 
	{
		assertEquals( "http://" + configuration.server() + "/api-" + configuration.api() + "/", client.getBaseUrl() );
	}
	
	@Test
	public void testGetBaseSslUrl()
	{
		assertEquals( "https://" + configuration.server() + "/api-" + configuration.api() + "/", client.getBaseSslUrl() );
	}

	@Test
	public void testGet() 
	{
		try
		{
			ISweebiData result = client.get( "server/info" );
			assertNotNull( result );
		}
		catch ( SweebiHttpException e )
		{
			/* this test may fail on internet */
			/* this isn't a real problem      */
			
			//TODO: configure client with internet server
		}
	}
	
	@Test
	public void testGetInvalidServer()
	{
	   
	    SweebiHttpClient client = new SweebiHttpClient( "http://noserver.nowhere.net:1234/" );
	    try
	    {
	        client.get( "foo" );
	        fail( "an exception should have been raised." );
	    }
	    catch ( SweebiHttpException e )
	    {
	        /* success */
	    }
	}
	


	@Test
	public void testGetHttpClient() throws IOException 
	{
		CloseableHttpClient temp = client.getHttpClient();
		assertNotNull( temp );
		temp.close();
	}

	public void testGetHttpsClient() throws IOException
	{
		CloseableHttpClient temp = client.getHttpsClient();
		assertNotNull( temp );
		temp.close();
	}
	
	@Test
	public void testExecute() throws ClientProtocolException, IOException, SweebiHttpException 
	{
		try 
		{
			client.execute( null, client.getHttpClient() );
			fail( "an exception should have been raised." );
		} 
		catch ( IllegalArgumentException e) 
		{
			/* success */
		}
	}
	
	@Test
	public void testToken()
	{
		assertNull( client.getToken() );
		assertFalse( client.hasToken() );
		
		client.setToken( "mytoken" );
		
		assertEquals( "mytoken", client.getToken() );
		assertTrue( client.hasToken() );
		
		client.setToken( null );
		
		assertNull( client.getToken() );
		assertFalse( client.hasToken() );
	}

}
