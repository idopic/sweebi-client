package com.sweebi.http.impl;

import static org.junit.Assert.*;

import javax.net.ssl.SSLSession;

import org.junit.Test;

public class SweebiAllHostsVerifierTest {

    @Test
    public void testSweebiAllHostsVerifier() 
    {
        assertNotNull( new SweebiAllHostsVerifier() );
    }

    @Test
    public void testVerify() 
    {
        assertTrue( new SweebiAllHostsVerifier().verify( (String)null, (SSLSession) null ) );
    }

}
