/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */
package com.sweebi.data.impl;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.sweebi.data.impl.SweebiImage;

public class SweebiImageTest 
{

	private static final byte[] BYTES = { 0, 1, 2, 3 };
	
	/**
	 * image under test.
	 */
	private SweebiImage image = null;
	
	@Before
	public void setUp() throws Exception 
	{
		this.image = new SweebiImage( BYTES );
	}

	@After
	public void tearDown() throws Exception 
	{
		this.image = null;
	}

	@Test
	public void testSweebiImageByteArray() 
	{
		assertNotNull( image );
		
		byte[] bad1 = null;
		try
		{
			new SweebiImage( bad1 );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
		
		byte[] bad2 = {};
		try
		{
			new SweebiImage( bad2 );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
	}

	@Test
	public void testSweebiImageFile() throws IOException 
	{
		File file = null;
		try
		{
			new SweebiImage( file );
			fail( "an exception should have been raised." );
		}
		catch ( IllegalArgumentException e )
		{
			/* success */
		}
		
		file = new File( "/toto.gif" );
		try
		{
			new SweebiImage( file );
			fail( "an exception should have been raised." );
		}
		catch ( FileNotFoundException e )
		{
			/* success */
		}
		
		file = new File( SweebiImageTest.class.getResource( "/S11.jpg" ).getPath() );
		SweebiImage sImage = new SweebiImage( file );
		
		assertEquals( 620004, sImage.asBytes().length );
		
		
	}

	@Test
	public void testAsBytes() 
	{
		assertNotNull( image.asBytes() );
		assertEquals( 4, image.asBytes().length );
		assertEquals( 3, image.asBytes()[3] );
	}

}
