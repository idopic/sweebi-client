/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */
package com.sweebi.utils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.apache.log4j.lf5.util.StreamUtils;

import com.sweebi.data.ISweebiImage;
import com.sweebi.data.impl.SweebiImage;

/**
 * some utilities for {@link ISweebiImage} instances.
 * @author Vincent ROSSIGNOL
 * @since com.idopic.sweebi-simple version 0.0.1
 * @version 0.0.1
 */
public class SweebiImageUtils 
{

    /**
     * internal logger.
     */
    private static final Logger LOGGER = Logger.getLogger( SweebiImageUtils.class );

    /**
     * constructor.
     * @throws UnsupportedOperationException always.
     */
    protected SweebiImageUtils() 
    {
        throw new UnsupportedOperationException( "no SweebiImageUtils' instances can be built." );
    }

    /**
     * reads bytes from the given file.
     * @param file a valid {@link File}'s instance.
     * @return bytes read from file.
     * @throws IOException if an I/O error occurred.
     */
    public static byte[] readBytes( File file ) throws IOException
    {
        if ( file == null )
        {
            throw new IllegalArgumentException( "invalid file: null ins't allowed." );
        }
        return readBytes( new FileInputStream( file ) );
    }

    /**
     * reads all bytes from the given stream.
     * @param input a valid {@link InputStream}'s instance.
     * @return bytes read from input stream.
     * @throws IOException if an I/O error occurred.
     */
    public static byte[] readBytes( InputStream input ) throws IOException
    {
        return StreamUtils.getBytes( input );
    }

    /**
     * creates a {@link ISweebiImage} for the given {@link BufferedImage}'s 
     * 	instance.
     * @param image a valid {@link BufferedImage}'s instance.
     * @return a valid {@link ISweebiImage}'s instance.
     */
    public static ISweebiImage readBufferedImage( BufferedImage image )
    {
        ByteArrayOutputStream ostream = new ByteArrayOutputStream();
        try 
        {
            ImageIO.write( image, "jpg", ostream );
            ostream.flush();
        }
        catch ( IOException e )
        {
            throw new IllegalStateException( "an I/O error occured while preparing buffer.", e );
        }

        ISweebiImage result = new SweebiImage( ostream.toByteArray() );

        try 
        {
            ostream.close();
        } 
        catch (IOException e) 
        {
            LOGGER.warn( "an I/O error occurred while close buffer.", e );
        }

        return result;
    }

    /**
     * builds and returns the {@link BufferedImage} corresponding to the given
     * 	{@link ISweebiImage}'s instance.
     * @param image a valid {@link ISweebiImage}'s instance.
     * @return a valid {@link BufferedImage}'s instance.
     * @throws IOException if an I/O error occurred.
     */
    public static BufferedImage toBufferedImage( ISweebiImage image ) throws IOException
    {
        if ( image == null )
        {
            throw new IllegalArgumentException( "invalid image: null isn't allowed." );
        }

        ByteArrayInputStream istream = new ByteArrayInputStream( image.asBytes() );
        return ImageIO.read( istream );
    }

}
