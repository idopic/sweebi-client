/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */
package com.sweebi.http.impl;

import java.io.Closeable;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.net.ssl.SSLContext;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import com.sweebi.data.ISweebiData;
import com.sweebi.data.ISweebiImage;
import com.sweebi.data.impl.SweebiData;
import com.sweebi.exceptions.SweebiHttpException;
import com.sweebi.http.ISweebiHttpClient;

/**
 * a {@link ISweebiHttpClient} implementation based on
 * 	Apache HTTP client library.
 * @author Vincent ROSSIGNOL
 * @since com.idopic.sweebi-simple version 0.0.1
 * @version 0.0.1
 */
public class SweebiHttpClient implements ISweebiHttpClient 
{

    /**
     * internal logger.
     */
    private static final Logger LOGGER = Logger.getLogger( SweebiHttpClient.class );

    /**
     * server's base URL.
     */
    private final String baseurl;

    /**
     * connection factory used for SSL clients.
     */
    private SSLConnectionSocketFactory factory = null;

    /**
     * {@link HostnameVerifier} to be used for SSL clients.
     * <p>
     * if this member is <code>null</code>, default host name verifier
     *  will be used.
     * </p>
     */
    //private X509HostnameVerifier hostnameVerifier = null;
    
    /**
     * Idopic token
     */
    private String token = null;

    /**
     * constructor.
     * @param url server base url.
     */
    public SweebiHttpClient( String url ) 
    {
        this(url, false );
    }
    
    /**
     * constructor.
     * @param url server base url.
     * @param allowAllHosts <code>true</code> if host names must correspond to
     *  Sweebi server certificate. <code>false</code> if no verification should
     *  be done.
     */
    public SweebiHttpClient( String url, boolean allowAllHosts ) 
    {
        super();
        if ( url == null || url.isEmpty() )
        {
            throw new IllegalArgumentException( "invalid base URL: null or empty isn't allowed." );
        }
        this.baseurl = url;

        this.initializeSSLContext( allowAllHosts );
        

    }
    
    
    /**
     * builds and returns the HTTP entity corresponding to the given content.
     * @param content a valid {@link ISweebiData}'s instance;
     * @return the corresponding {@link HttpEntity}'s instance.
     * @throws SweebiHttpException if an error occurred.
     */
    private final HttpEntity buildEntity( ISweebiData content ) throws SweebiHttpException
    {
        if ( content == null )
        {
            throw new IllegalArgumentException( "invalid data content: null isn't allowed." );
        }

        try
        {
            StringEntity entity = new StringEntity( content.toJsonString() );
            entity.setContentType( JSON_CONTENT );
            return entity;
        }
        catch ( UnsupportedEncodingException e )
        {
            throw new SweebiHttpException( "failed setting JSON content", e );
        }

    }

    /**
     * builds and returns the HTTP entity corresponding to the given content.
     * @param content a valid {@link ISweebiImage}'s instance;
     * @return the corresponding {@link HttpEntity}'s instance.
     * @throws SweebiHttpException if an error occurred.
     */
    private final HttpEntity buildEntity( ISweebiImage content ) throws SweebiHttpException
    {
        if ( content == null )
        {
            throw new IllegalArgumentException( "invalid data content: null isn't allowed." );
        }

        ByteArrayEntity entity = new ByteArrayEntity( content.asBytes() );
        entity.setContentType( JPEG_CONTENT );
        return entity;


    }

    /**
     * silently closes a given {@link Closeable}'s object.
     * <p>
     * if an I/O exception occurred, it will only be logged as error.
     * </p>
     * @param closeable the instance to be closed.
     */
    private void close( Closeable closeable )
    {
        if ( closeable == null )
        {
            LOGGER.warn( "invalid object to be closed: null isn't allowed." );
        }
        else
        {
            try
            {
                closeable.close();
            }
            catch ( IOException e )
            {
                LOGGER.error( "an I/O error occurred during a close operation.", e );
            }
        }
    }

    /**
     * execute the given request and returns the result as a 
     * 	{@link ISweebiData}'s instance (build from received JSON object).
     * @param request a valid {@link HttpUriRequest}'s instance.
     * @return a valid {@link ISweebiData}'s instance.
     * @throws SweebiHttpException if an unattended response was received.
     * @throws IOException if an I/O error occurred.
     */
    protected ISweebiData execute( HttpUriRequest request, CloseableHttpClient client ) throws SweebiHttpException
    {
        String 					json 	   = null;
        ISweebiData 			data 	   = null;
        int						statusCode = 0;
        CloseableHttpResponse 	response   = null;

        /* setting Idopic token as header if it has been defined */
        if ( this.hasToken() )
        {
            request.setHeader( IDOPIC_TOKEN, this.getToken() );
        }

        try
        {
            response   = client.execute(request);
            statusCode = response.getStatusLine().getStatusCode();
            json       = EntityUtils.toString( response.getEntity() );

        }
        catch ( IOException e )
        {
            throw new SweebiHttpException( "an I/O error occured during HTTP request.", e );
        }
        finally
        {
            this.close( response );
            this.close( client   );
        }

        if ( statusCode != 500  && json != null && ! json.isEmpty() )
        {			
            data = SweebiData.forJsonString(json);
        }

        if ( statusCode != 200 )
        {
            LOGGER.warn( "server returned HTTP status " + statusCode + " with content " + json );
            throw new SweebiHttpException( "server responsed HTTP status " + statusCode, data );
        }

        return data;

    }

    @Override
    public ISweebiData get(String path) throws SweebiHttpException
    {
        this.testPath( path );
        
       
        
        String url = this.getBaseUrl() + path;
        HttpGet request = new HttpGet( url );
        
        LOGGER.info( "sending GET " + url );
        return this.execute( request, this.getHttpClient() );


    }

    /**
     * server's base SSL URL
     * @return base URL with HTTPS protocol
     */
    public final String getBaseSslUrl()
    {
        return this.baseurl.replaceAll("http:", "https:");
    }

    /**
     * server's base URL
     * @return base URL as a valid {@link String}'s instance.
     */
    public final String getBaseUrl()
    {
        return this.baseurl;
    }

    /**
     * returns a valid HTTP client.
     * @return a valid {@link CloseableHttpClient}'s instance.
     */
    protected CloseableHttpClient getHttpClient()
    {
        return HttpClients.createDefault();
    }

    /**
     * returns a valid HTTPS client
     * @return a vlaid {@link CloseableHttpClient}'s instance.
     */
    protected CloseableHttpClient getHttpsClient()
    {
        return HttpClients.custom().setSSLSocketFactory( this.factory ).build();
    }

    /**
     * associated Idopic token.
     * @return token as a {@link String}'s instance.
     */
    protected final String getToken()
    {
        return this.token;
    }

    /**
     * tests if an Idopic token has been set.
     * @return <code>true</code> if a token exists, <code>false</code>
     * 	otherwise.
     */
    protected final boolean hasToken()
    {
        return this.getToken() != null;
    }

    /**
     * initializes SSL context for SSL clients.
     */
    private final void initializeSSLContext( boolean allowAllHosts)
    {
        /* for Java 1.7 */
        System.setProperty("jsse.enableSNIExtension", "false");

   
        
        SSLContext context;
        try 
        {
                
            context = SSLContexts.custom().loadTrustMaterial( null,  new SweebiDefaultTrustStategy() ).build();
        } 
        catch ( Exception e )
        {
            throw new IllegalStateException( "failed initializing SSL context.", e );
        }
        
        if ( allowAllHosts )
        {
            LOGGER.warn( "no host name verification will be done on SSL connections.");
            this.factory = new SSLConnectionSocketFactory( context, new SweebiAllHostsVerifier() );
        }
        else
        {
            LOGGER.warn( "no certificate verification will be done on SSL connections.");
            this.factory = new SSLConnectionSocketFactory( context );
        }
    }

    @Override
    public ISweebiData post(String path, ISweebiData content) throws SweebiHttpException 
    {
        this.testPath( path );
        this.testContent( content );

        String url = this.getBaseUrl() + path;
        HttpPost request = new HttpPost( url );

        LOGGER.info( "sending POST " + url + " - with json: " + content.toJsonString() );

        request.setEntity( this.buildEntity( content ) );
        return this.execute( request, this.getHttpClient() );

    }

    @Override
    public ISweebiData postSSL(String path, ISweebiData content) throws SweebiHttpException 
    {
        this.testPath( path );
        String url = this.getBaseSslUrl() + path;

        LOGGER.info( "POST " + url ); 

        HttpPost request = new HttpPost( url );

        request.setEntity( this.buildEntity( content ) );

        return this.execute( request, this.getHttpsClient() );
    }

    @Override
    public ISweebiData put(String path, ISweebiImage image) throws SweebiHttpException 
    {
        HttpPut request = new HttpPut( this.getBaseSslUrl() + path );

        request.setEntity( this.buildEntity( image ) );

        return this.execute( request, this.getHttpsClient() );
    }

    @Override
    public ISweebiData putSSL(String path, ISweebiData content) throws SweebiHttpException 
    {
        this.testPath( path );
        HttpPut request = new HttpPut( this.getBaseSslUrl() + path );

        request.setEntity( this.buildEntity( content ) );

        return this.execute( request, this.getHttpsClient() );
    }

    @Override
    public void setToken(String token) 
    {
        this.token = token;
    }

    /**
     * tests if the given content is valid.
     * @param content a valid {@link ISweebiData}'s instance.
     * @throws IllegalArgumentException if the content isn't valid.
     */
    private final void testContent( ISweebiData content )
    {
        if ( content == null )
        {
            throw new IllegalArgumentException( "invalid content: null isn't allowed." );
        }
    }

    /**
     * tests if the given path is valid.
     * @param path the path to be tested.
     * @throws IllegalArgumentException if the path isn't valid.
     */
    private final void testPath( String path )
    {
        if ( path == null || path.isEmpty() )
        {
            throw new IllegalArgumentException( "invaldi path: null or empty string isn't allowed." );
        }
    }

}
