/**
 * 
 */
package com.sweebi.http.impl;

import java.io.IOException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;

import org.apache.http.conn.ssl.X509HostnameVerifier;

/**
 * an {@link HostnameVerifier}'s implementation which accepts all hosts.
 * <p>
 * this class has been designed for test purpose and should never be used
 *  in production.
 * </p>
 * @author Vincent ROSSIGNOL
 * @since com.sweebi.sweebi-simple version 0.0.1
 * @version 0.0.1
 */
public class SweebiAllHostsVerifier implements X509HostnameVerifier 
{

    /**
     * constructor.
     */
    public SweebiAllHostsVerifier() 
    {
        super();
    }

    @Override
    public void verify(String host, SSLSocket ssl) throws IOException 
    {
        /* no code */
    }

    @Override
    public void verify(String host, X509Certificate cert) throws SSLException 
    {
        /* no code */
    }

    @Override
    public void verify(String host, String[] cns, String[] subjectAlts) throws SSLException 
    {
        /* no code */
    }

    @Override
    public boolean verify(String hostname, SSLSession session) 
    {
        return true;
    }

}
