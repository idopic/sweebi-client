/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */
package com.sweebi.http.impl;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import org.apache.http.conn.ssl.TrustStrategy;

/**
 * {@link TrustStrategy} which permit auto-signed certificates.
 * @author Vincent ROSSIGNOL
 * @since com.idopic.sweebi-simple version 0.0.1
 * @version 0.0.1
 */
public class SweebiDefaultTrustStategy implements TrustStrategy 
{

    /**
     * constructor.
     */
    public SweebiDefaultTrustStategy() 
    {
        super();
    }


    @Override
    public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException 
    {
        /* all certificates are accepted */
        return true;
    }

}
