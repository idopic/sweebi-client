/*
 * (C) Copyright 2015 Idopic and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * Contributors:
 *    Vincent ROSSIGNOL
 */
package com.sweebi.data.impl;

import java.io.File;
import java.io.IOException;

import com.sweebi.data.ISweebiImage;
import com.sweebi.utils.SweebiImageUtils;

/**
 * {@link ISweebiImage}'s implementation.
 * @author Vincent ROSSIGNOL
 * @since com.idopic.sweebi-simple version 0.0.1
 * @version 0.0.1
 */
public class SweebiImage implements ISweebiImage 
{

    /**
     * associated bytes.
     */
    private final byte[] bytes;


    /**
     * constructor.
     */
    public SweebiImage( byte[] bytes ) 
    {
        super();

        if ( bytes == null || bytes.length == 0 )
        {
            throw new IllegalArgumentException( "invalid bytes: null or emply isn't allowed." );
        }
        this.bytes = bytes;
    }

    /**
     * constructor.
     * @param image a valid JPEG file.
     * @throws IOException if an I/O error occurred.
     */
    public SweebiImage( File image ) throws IOException
    {
        this( SweebiImageUtils.readBytes( image ) );
    }

    @Override
    public byte[] asBytes() 
    {
        return this.bytes;
    }

}
